using System;
using System.Collections.Generic;
using Xunit;

namespace TFMModeling.Tests
{
    public class CardTests
    {
        public Card CardCreationHelper(
                string bottomText="Empty",
                int cost=0,
                string name="Card",
                Card.EXPANSIONS expansion=Card.EXPANSIONS.Basic,
                Card.DECK_TYPES deck = Card.DECK_TYPES.Project,
                Card.TYPES type = Card.TYPES.Auto,
                List<Card.TAG_NAMES> tags = null,
                Card.RESOURCE_TYPES resourceType = Card.RESOURCE_TYPES.None,
                string topText="What it does",
                CardVPConditions vpCondition = null)
        {
            return new Card(bottomText, cost, name, expansion, deck, type, tags, resourceType, topText, vpCondition);
        }
        
        [Fact]
        public void InitializationCheck()
        {
            Card card = CardCreationHelper();
            Assert.NotNull(card);
        }

        [Theory]
        [InlineData(99)]
        [InlineData(1)]
        [InlineData(0)]
        public void CostGetterSetterValidCheck(int value)
        {
            Card card = CardCreationHelper(cost: value);
            Assert.True(value == card.Cost);
        }

        [Theory]
        [InlineData(100)]
        [InlineData(-1)]
        public void CostGetterSetterInValidCheck(int value)
        {
            Assert.Throws<Exception>(() => CardCreationHelper(cost: value));
        }

        [Theory]
        [InlineData("ValidName")]
        [InlineData("1")]
        [InlineData(".")]
        [InlineData("String with spaces")]
        [InlineData("Name length that is just barely allowed.")]
        public void NameGetterSetterValidCheck(string value)
        {
            Card card = CardCreationHelper(name: value);
            Assert.True(value == card.Name);
        }

        [Theory]
        [InlineData("This was just barely longer than allowed.")]
        [InlineData("")]
        public void NameGetterSetterInValidCheck(string value)
        {
            Assert.Throws<Exception>(() => CardCreationHelper(name: value));
        }

        [Theory]
        [InlineData(Card.EXPANSIONS.Basic)]
        [InlineData(Card.EXPANSIONS.Prelude)]
        public void ExpansionGetterSetterValidCheck(Card.EXPANSIONS value)
        {
            Card card = CardCreationHelper(expansion: value);
            Assert.True(value == card.Expansion);
        }

        [Theory]
        [InlineData(Card.DECK_TYPES.Corp)]
        [InlineData(Card.DECK_TYPES.Project)]
        public void DeckGetterSetterValidCheck(Card.DECK_TYPES value)
        {
            Card card = CardCreationHelper(deck: value);
            Assert.True(value == card.Deck);
        }

        public static IEnumerable<object[]> TagsData =>
        new List<object[]>
        {
            new object[] { new List<Card.TAG_NAMES> { Card.TAG_NAMES.Building, }, },
            new object[] { new List<Card.TAG_NAMES> {  }, },
            new object[] { new List<Card.TAG_NAMES> { Card.TAG_NAMES.Building, Card.TAG_NAMES.Building, Card.TAG_NAMES.Jovian, }, },
        };

        [Theory]
        [MemberData(nameof(TagsData))]
        public void TagGetterValidCheck(List<Card.TAG_NAMES> values)
        {
            Card card = CardCreationHelper(tags: values);
            Assert.True(values.Count == card.Tags.Count);
        }

        [Theory]
        [InlineData(Card.RESOURCE_TYPES.Animal)]
        [InlineData(Card.RESOURCE_TYPES.None)]
        public void ResourceTypeGetterSetterValidCheck(Card.RESOURCE_TYPES value)
        {
            Card card = CardCreationHelper(resourceType: value);
            Assert.True(value == card.ResourceType);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(10)]
        [InlineData(999)]
        public void ResourceCountGetterSetterValidCheck(int value)
        {
            Card card = CardCreationHelper();
            card.ResourceCount = value;
            Assert.True(value == card.ResourceCount);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-999)]
        public void ResourceCountGetterSetterInValidCheck(int value)
        {
            Card card = CardCreationHelper();
            card.ResourceCount = value;
            Assert.True(0 == card.ResourceCount);
        }

        public static IEnumerable<object[]> ValidStringsData =>
        new List<object[]>
        {
            new object[] { "ValidString", },
            new object[] { "1", },
            new object[] { ".", },
            new object[] { "String with spaces", },
            new object[] { "!@#$%^&*()", },
            new object[] { "", },
        };

        [Theory]
        [MemberData(nameof(ValidStringsData))]
        public void BottomStringSetterCheck(string value)
        {
            Card card = CardCreationHelper(bottomText: value);
            Assert.True(value == card.BottomText);
        }

        [Theory]
        [MemberData(nameof(ValidStringsData))]
        public void TopStringSetterCheck(string value)
        {
            Card card = CardCreationHelper(topText: value);
            Assert.True(value == card.TopText);
        }

        public static IEnumerable<object[]> VPConditionsData =>
        new List<object[]>
        {
            new object[] { new CardVPConditions(0, 1, CardVPConditions.ConditionalTypes.None), },
            new object[] { new CardVPConditions(2, 2, CardVPConditions.ConditionalTypes.CountJovianTags), },
        };

        [Theory]
        [MemberData(nameof(VPConditionsData))]
        public void VPConditionGetterValidCheck(CardVPConditions value)
        {
            Card card = CardCreationHelper(vpCondition: value);
            Assert.True(value == card.VPCondition);
        }
    }
}
