using System;
using System.Collections.Generic;
using Xunit;

namespace TFMModeling.Tests
{
    public class CardRequirementsTests
    {
        private readonly Tuple<int, string>[] defaultTuple = new Tuple<int, string>[] { Tuple.Create(0, "") };
        public CardRequirements CardRequirementsCreationHelper(
                bool isCutoff = false,
                CardRequirements.Types types = CardRequirements.Types.DegreesCelsius,
                Tuple<int, string>[] modifiers = null)
        {
            if (modifiers == null)
            {
                modifiers = defaultTuple;
            }
            return CardRequirementsCreationHelperRaw(isCutoff, types, modifiers);
        }
        public CardRequirements CardRequirementsCreationHelperRaw(
                bool isCutoff = false,
                CardRequirements.Types types = CardRequirements.Types.DegreesCelsius,
                Tuple<int, string>[] modifiers = null)
        {
            return new CardRequirements(isCutoff, types, modifiers);
        }

        [Fact]
        public void InitializationCheck()
        {
            CardRequirements cardRequirements = CardRequirementsCreationHelper();
            Assert.NotNull(cardRequirements);
        }

        [Fact]
        public void ModifiersNullCheck()
        {
            Assert.Throws<Exception>(() => CardRequirementsCreationHelperRaw());
        }

        [Fact]
        public void ModifiersInvalidCheck()
        {
            Assert.Throws<Exception>(() => CardRequirementsCreationHelperRaw(modifiers: new Tuple<int,string>[] {}));
        }
    }
}