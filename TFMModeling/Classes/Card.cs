using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TFMModeling
{
    /// <summary>
    /// Base class representing a generic card
    /// </summary>
    public class Card
    {
        private readonly int maxCost = 99;
        private readonly int minCost = 0;
        private readonly int maxNameLength = 40;

        private int resourceCount = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="Card"/> class.
        /// </summary>
        /// <param name="bottomText">Flavor Text on the bottom of the card</param>
        /// <param name="cost">Cost to play the card</param>
        /// <param name="name">Name of the card</param>
        /// <param name="expansion">Expansion card can be found in</param>
        /// <param name="deck">Deck the card comes out of</param>
        /// <param name="type">Type of the card</param>
        /// <param name="tags">Tags present on the card</param>
        /// <param name="resourceType">Reource type this card uses</param>
        /// <param name="topText">Text on the top describing what the card does</param>
        /// <param name="vpCondition">The VP this card is worth and its conditions</param>
        public Card(
                string bottomText,
                int cost,
                string name,
                EXPANSIONS expansion,
                DECK_TYPES deck,
                TYPES type,
                List<TAG_NAMES> tags,
                RESOURCE_TYPES resourceType,
                string topText,
                CardVPConditions vpCondition)
        {
            this.BottomText = bottomText;
            this.Expansion = expansion;
            this.Deck = deck;
            this.Type = type;
            this.ResourceType = resourceType;
            this.TopText = topText;
            this.VPCondition = vpCondition;
            if (name != null && name.Length > 0 && name.Length <= this.maxNameLength)
            {
                this.Name = name;
            }
            else
            {
                throw new Exception("Invalid name provided");
            }

            if (cost <= this.maxCost && cost >= this.minCost)
            {
                this.Cost = cost;
            }
            else
            {
                throw new Exception("Invalid Cost");
            }

            if (tags != null)
            {
                this.Tags = tags.AsReadOnly();
            }
            else
            {
                this.Tags = new ReadOnlyCollection<TAG_NAMES>(new List<TAG_NAMES>());
            }
        }

        /// <summary>
        /// The type of card which determines when the card is used/handled
        /// </summary>
        public enum DECK_TYPES
        {
            /// <summary>
            /// Your corporation, which affects your starting situation and the bonuses you being the game with
            /// </summary>
            Corp,

            /// <summary>
            /// Extra cards given at the beginning of the game to speed up early gameplay
            /// </summary>
            Prelude,

            /// <summary>
            /// The main card type seen in the gameplay which is used to progress the game
            /// </summary>
            Project,
        }

        /// <summary>
        /// Enum represting the various expansions currently supported (indicated in game by special symbols on the cards)
        /// </summary>
        public enum EXPANSIONS
        {
            /// <summary>
            /// The Basic cards. Good for beginner game
            /// </summary>
            Basic,

            /// <summary>
            /// Corporate cards included for harder games in base
            /// </summary>
            CorpEra,

            /// <summary>
            /// Don't know, fill in later
            /// </summary>
            VenusNext,

            /// <summary>
            /// Deck meant to help the early game progress faster
            /// </summary>
            Prelude,

            /// <summary>
            /// Don't know, fill in later
            /// </summary>
            Colonies,

            /// <summary>
            /// Don't know, fill in later
            /// </summary>
            Turmoil,

            /// <summary>
            /// Promotional cards given away at/for special events/occasions
            /// </summary>
            Promo,
        }

        /// <summary>
        /// The kinds of resources that can be stored on a card
        /// </summary>
        public enum RESOURCE_TYPES
        {
            /// <summary>
            /// Animal
            /// </summary>
            Animal,

            /// <summary>
            /// Microbe
            /// </summary>
            Microbe,

            /// <summary>
            /// Fighter
            /// </summary>
            Fighter,

            /// <summary>
            /// Science
            /// </summary>
            Science,

            /// <summary>
            /// Floater
            /// </summary>
            Floater,

            /// <summary>
            /// Asteroid
            /// </summary>
            Asteroid,

            /// <summary>
            /// Camp
            /// </summary>
            Camp,

            /// <summary>
            /// None
            /// </summary>
            None,
        }

        /// <summary>
        /// The possible types of tags a card can have (represented by the icons in the top right of the card)
        /// </summary>
        public enum TAG_NAMES
        {
            /// <summary>
            /// Building
            /// </summary>
            Building,

            /// <summary>
            /// Space
            /// </summary>
            Space,

            /// <summary>
            /// City
            /// </summary>
            City,

            /// <summary>
            /// Power
            /// </summary>
            Power,

            /// <summary>
            /// Plant
            /// </summary>
            Plant,

            /// <summary>
            /// Microbe
            /// </summary>
            Microbe,

            /// <summary>
            /// Animal
            /// </summary>
            Animal,

            /// <summary>
            /// Science
            /// </summary>
            Science,

            /// <summary>
            /// Earth
            /// </summary>
            Earth,

            /// <summary>
            /// Jovian
            /// </summary>
            Jovian,

            /// <summary>
            /// Venus
            /// </summary>
            Venus,

            /// <summary>
            /// Wild
            /// </summary>
            Wild,
        }

        /// <summary>
        /// The type of the card (indicated in game by its colored border)
        /// </summary>
        public enum TYPES
        {
            /// <summary>
            /// Your corporation, which affects your starting situation and the bonuses you being the game with
            /// </summary>
            Corp,

            /// <summary>
            /// Extra cards given at the beginning of the game to speed up early gameplay
            /// </summary>
            Prelude,

            /// <summary>
            /// Cards that have have a persistent effect in the game
            /// </summary>
            Auto,

            /// <summary>
            /// Cards that have an active ability of some kind
            /// </summary>
            Active,

            /// <summary>
            /// One time use cards that only have an affect when played and at the end of the game
            /// </summary>
            Event,
        }

        /// <summary>
        /// Gets the text displayed at the bottom of the card, usually describes the overall effects of the cards
        /// </summary>
        public string BottomText { get; } = string.Empty;

        /// <summary>
        /// Gets the amount of currency that must be spent to `play` the card
        /// </summary>
        public int Cost { get; } = 0;

        /// <summary>
        /// Gets the type of card this is.
        /// </summary>
        public DECK_TYPES Deck { get; } = DECK_TYPES.Project;

        /// <summary>
        /// Gets the expansion this card belongs to.
        /// </summary>
        public EXPANSIONS Expansion { get; } = EXPANSIONS.Basic;

        /// <summary>
        /// Gets the name string of the card
        /// </summary>
        public string Name { get; } = string.Empty;

        /// <summary>
        /// Gets or sets the number of resources this card is currently holding
        /// </summary>
        public int ResourceCount
        {
            get
            {
                return this.resourceCount;
            }

            set
            {
                if (value > 0)
                {
                    this.resourceCount = value;
                }
            }
        }

        /// <summary>
        /// Gets the type of resource this card can hold
        /// </summary>
        public RESOURCE_TYPES ResourceType { get; } = RESOURCE_TYPES.None;

        /// <summary>
        /// Gets the list of editable list tags (there can be no tags as well as mutliple of the same)
        /// </summary>
        public ReadOnlyCollection<TAG_NAMES> Tags { get; }

        /// <summary>
        /// Gets the text displayed at the top of the card, usually describes the actions that can be taken
        /// </summary>
        public string TopText { get; } = string.Empty;

        /// <summary>
        /// Gets the card type.
        /// </summary>
        public TYPES Type { get; } = TYPES.Auto;

        /// <summary>
        /// Gets the VPCondition for this card
        /// </summary>
        public CardVPConditions VPCondition { get; }
    }
}