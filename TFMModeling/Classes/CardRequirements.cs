using System;

namespace TFMModeling
{
    /// <summary>
    /// Base class representing the play requirements a card may have
    /// Excludes Turmoil Expansion as I don't know how that works
    /// </summary>
    public class CardRequirements
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardRequirements"/> class.
        /// </summary>
        /// <param name="isCutoff">if the requirements is cutoff else minimum</param>
        /// <param name="type">the <see cref="Types"/> type of this requirement</param>
        /// <param name="modifiers">tuple array where int = value/count and string = type of tiles/tags/etc</param>
        public CardRequirements(bool isCutoff, Types type, Tuple<int, string>[] modifiers)
        {
            if (modifiers == null || modifiers.Length == 0)
            {
                throw new Exception("Modifiers must be provided for valid Card Requirements");
            }

            this.IsCutoff = isCutoff;
            this.Type = type;
            this.Modifiers = modifiers;
        }

        /// <summary>
        /// Defines the Types of requirements possible to be represented by this class
        /// </summary>
        public enum Types
        {
            /// <summary>
            /// Relies on a percentage value (either Oxygen or Venus)
            /// </summary>
            Percentage,

            /// <summary>
            /// Relies on some number of tile types being in play (ex: City and Greenery)
            /// </summary>
            Tile,

            /// <summary>
            /// Degrees Celsius on the board
            /// </summary>
            DegreesCelsius,

            /// <summary>
            /// Relies on some number of tags in players play area
            /// </summary>
            Tag,

            /// <summary>
            /// Relies on some type of production constraints
            /// </summary>
            Production,

            /// <summary>
            /// Relies on having cards with some kind of resource available
            /// </summary>
            Resource,

            /// <summary>
            /// Relies on the player's TerraForm Rating
            /// </summary>
            TerraFormRating,
        }

        /// <summary>
        /// Gets a value indicating whether this requirement is a cutoff, otherwise its considered a minimum
        /// </summary>
        public bool IsCutoff { get; } = false;

        /// <summary>
        /// Gets the array of tuple values representing the Type modifiers: int = value/count and string = type of tiles/tags/etc
        /// </summary>
        public Tuple<int, string>[] Modifiers { get; }

        /// <summary>
        /// Gets the type of requirement this card has
        /// </summary>
        public Types Type { get; }
    }
}