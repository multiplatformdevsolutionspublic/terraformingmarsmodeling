using System;
using Xunit;

namespace TFMModeling.Tests
{
    public class CardVPConditionsTests
    {
        public CardVPConditions CvpcCreationHelper(
                int vpRaw = 0,
                int conditionalCount = 1,
                CardVPConditions.ConditionalTypes conditionalType = CardVPConditions.ConditionalTypes.None)
        {
            return new CardVPConditions(vpRaw, conditionalCount, conditionalType);
        }
        [Fact]
        public void InitializationCheck()
        {
            CardVPConditions cvpc = CvpcCreationHelper();
            Assert.NotNull(cvpc);
        }

        [Theory]
        [InlineData(99)]
        [InlineData(1)]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-99)]
        public void VPRawGetterValidCheck(int value)
        {
            CardVPConditions cvpc = CvpcCreationHelper(vpRaw: value);
            Assert.True(value == cvpc.VictoryPointsRaw);
        }

        [Theory]
        [InlineData(99)]
        [InlineData(1)]
        public void ConditionalCountGetterValidCheck(int value)
        {
            CardVPConditions cvpc = CvpcCreationHelper(conditionalCount: value);
            Assert.True(value == cvpc.ConditionalCount);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-99)]
        public void ConditionalCountGetterInValidCheck(int value)
        {
            Assert.Throws<Exception>(() => CvpcCreationHelper(conditionalCount: value));
        }

        [Theory]
        [InlineData(CardVPConditions.ConditionalTypes.None)]
        [InlineData(CardVPConditions.ConditionalTypes.AtLeastOneResourceHere)]
        [InlineData(CardVPConditions.ConditionalTypes.CountResourceHere)]
        [InlineData(CardVPConditions.ConditionalTypes.CountAdjacentCityTiles)]
        public void ExpansionGetterSetterValidCheck(CardVPConditions.ConditionalTypes value)
        {
            CardVPConditions cvpc = CvpcCreationHelper(conditionalType: value);
            Assert.True(value == cvpc.ConditionalType);
        }
    }
}