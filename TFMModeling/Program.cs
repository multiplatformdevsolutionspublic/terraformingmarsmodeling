﻿using System;

namespace TFMModeling
{
    /// <summary>
    /// The main program that will handle all application functionality
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main application function that will handle kicking off all other functionality
        /// </summary>
        /// <param name="args">command line parameters being passed in</param>
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}