using System;

namespace TFMModeling
{
    /// <summary>
    /// Base class representing the VP conditions a card may have
    /// </summary>
    public class CardVPConditions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardVPConditions"/> class.
        /// </summary>
        /// <param name="victoryPointsRaw">Number of raw Victory Points that will be modified by conditions</param>
        /// <param name="conditionalCount">Number to divide count of met conditions my to determine point modifier</param>
        /// <param name="conditionalType">The type of condition that will be counted/evaluated</param>
        public CardVPConditions(int victoryPointsRaw, int conditionalCount, ConditionalTypes conditionalType)
        {
            this.VictoryPointsRaw = victoryPointsRaw;

            if (conditionalCount >= 1)
            {
                this.ConditionalCount = conditionalCount;
            }
            else
            {
                throw new Exception("Invalid Conditional Count provided");
            }

            this.ConditionalType = conditionalType;
        }

        /// <summary>
        /// Enum representing the types of conditions that may be used to determine victory point total
        /// </summary>
        public enum ConditionalTypes
        {
            /// <summary>
            /// Score modified by the number of resources on this card
            /// </summary>
            CountResourceHere,

            /// <summary>
            /// Score modified if at least one resource here
            /// </summary>
            AtLeastOneResourceHere,

            /// <summary>
            /// Score modified by the number of Jovian tags in this hand
            /// </summary>
            CountJovianTags,

            /// <summary>
            /// Score modified by the number of Colonies on the map
            /// </summary>
            CountColonies,

            /// <summary>
            /// Score modified by the count of City Tiles adjacent to this card's tile
            /// </summary>
            CountAdjacentCityTiles,

            /// <summary>
            /// Score modified by the count of City Tiles on the map
            /// </summary>
            CountCityTiles,

            /// <summary>
            /// Score modified by the count of Ocean Tiles adjacent to this card's tile
            /// </summary>
            CountAdjacentOceanTiles,

            /// <summary>
            /// No conditions for the Victory Points on this card
            /// </summary>
            None,
        }

        /// <summary>
        /// Gets the number of Victory Points to be modified with any conditions or modifiers a card is worth
        /// </summary>
        public int VictoryPointsRaw { get; } = 0;

        /// <summary>
        /// Gets the number of condtional items required
        /// </summary>
        public int ConditionalCount { get; } = 0;

        /// <summary>
        /// Gets the type of this cards conditional rewards
        /// </summary>
        public ConditionalTypes ConditionalType { get; } = ConditionalTypes.None;
    }
}