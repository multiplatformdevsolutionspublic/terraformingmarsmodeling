## Introduction

This is a simple pipeline example for a .NET Core application, showing just
how easy it is to get up and running with .NET development using GitLab.

## Build Status
![pipeline status](https://gitlab.com/multiplatformdevsolutionspublic/terraformingmarsmodeling/badges/master/pipeline.svg)

## Documentation

Self hosted at the our pages site: https://multiplatformdevsolutionspublic.gitlab.io/terraformingmarsmodeling/